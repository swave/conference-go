from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):

    headers = {"Authorization": PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/search?query={city},{state}"
    response = requests.get(url, headers=headers)
    data = json.loads(response.text)
    picture_url= data["photos"][0]["src"]["original"]

    return{"picture_url": picture_url}



def get_weather_data(city, state):

    geocoding_url = f"https://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}"
    geocoding_response = requests.get(geocoding_url)
    geocoding_data = geocoding_response.json()
    
    lat = geocoding_data[0]['lat']
    lon = geocoding_data[0]['lon']
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    weather_response = requests.get(weather_url)
    weather_data = weather_response.json()
    weather_dict = {
        "temperature": weather_data['main']['temp'],
        "description": weather_data['weather'][0]['description'],
    }
    return weather_dict
